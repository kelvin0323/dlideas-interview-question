<?php

namespace App\Libraries;

class Message
{
	public $message = "Nice to Meet You!";

	public function display_message(string $company_name)
	{
		return "Hi {$company_name} .".$this->message;
	}
}