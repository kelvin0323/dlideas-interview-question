<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Libraries\Message;
use App\Http\Resources\TodoResource;

class InterviewQuestionController extends Controller
{
    public function question_1a(Request $request)
    {
    	//retreive api data
    	$todo_users = json_decode(Http::get('https://jsonplaceholder.typicode.com/users'),true);

    	//convert todos to collection format for use group by method
    	$todos = collect(json_decode(Http::get('https://jsonplaceholder.typicode.com/todos'),true));

    	//seperate incompleted and completed todo
    	$todos = $todos->groupBy('completed');

    	//take completed todo only
    	foreach($todos[1]->groupBy('userId') as $user_id => $todo_user_list){
    		foreach($todo_users as $key => $user) if($user['id'] == $user_id){
    			//assign number of todos into users 
	    		$todo_users[$key]['count'] = count($todo_user_list);
    		}
    	}

    	//convert to collection for use sort method
    	//order by todo count in assending order 
    	$todo_users = collect($todo_users)->sortBy('count');	 

    	return view('question_one',compact('todo_users'));
    }

    public function question_1b(Request $request)
    {
    	//retreive data from api
    	$todos = collect(json_decode(Http::get('https://jsonplaceholder.typicode.com/todos'),true));

    	//if allow user search userId then put in this array
    	//if disallow then remove from this array
    	
    	//test link
    	//http://127.0.0.1:8000/api/interview-question/1b/search?userId=3&completed=true&title=quis
    	$searchable_fields = [
    		'completed',
    		'title',
    		'userId',
    	];

    	foreach($searchable_fields as $field){
    		$todos = $todos->when($request->$field, function($q) use ($field){
    			//filter title 
    			if($field == 'title'){
    				return $q->filter(function ($todo){
    					return false !== stristr($todo['title'], request('title'));
    				});
    			}
    			//filter completed convert to bool format
    			if($field == 'completed'){
    				return $q->where($field,filter_var(request($field), FILTER_VALIDATE_BOOLEAN));
    			}

    			//other field
    			return $q->where($field,request($field));
    		});
    	}

    	//unmatched result
    	if(!$todos->count()){
    		return response()->json(['message' => 'Empty Result!'],200);
    	}

    	$todos = new TodoResource($todos);

    	$todos = json_encode($todos, JSON_PRETTY_PRINT);

    	echo "<pre>" . $todos . "</pre>";

    }

    //Interview question 2
    public function question_2(Request $request)
    {
    	//The coding of this class can refer the App\Libraries\Message
    	$company_name = 'DL Ideas';
		$message = New Message();
		echo $message->display_message($company_name);
    }

}
