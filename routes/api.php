<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//test link
//http://127.0.0.1:8000/api/interview-question/1b/search?completed=true&title=earum
Route::group(['prefix' => 'interview-question'], function(){
	Route::get('1b/search/','InterviewQuestionController@question_1b');
});
