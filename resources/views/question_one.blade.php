<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css">
    <link href="https://cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.4/css/dataTables.responsive.css"/>

    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }
    </style>
</head>
<body>
    <div class="text-center">
       <h2>Interview Question 1a - Todo User List</h2>    
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-bordered table-hover dt-responsive">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Number of completed todo</th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th width="20%">Address</th>
                            <th>Phone</th>
                            <th>WebSite</th>
                            <th width="20%">Company</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($todo_users as $user)
                        <tr>
                            <td>{{$user['id']}}</td>
                            <td>{{$user['count']}}</td>
                            <td>{{$user['name']}}</td>
                            <td>
                                {{$user['username']}}
                            </td>
                            <td>
                                {{$user['email']}}
                            </td>
                            <td>
                                Stree: <b>{{$user['address']['street']}}</b> <br>
                                Suite: <b>{{$user['address']['suite']}}</b> <br>
                                City: <b>{{$user['address']['city']}}</b> <br>
                                Zipcode: <b>{{$user['address']['zipcode']}}</b> <br>
                                Geo: <b>{{$user['address']['geo']['lat']}},{{$user['address']['geo']['lng']}}</b> <br>
                            </td>
                            <td>{{$user['phone']}}</td>
                            <td>{{$user['website']}}</td>
                            <td>
                                Name: <b>{{$user['company']['name']}}</b> <br>
                                CatchPhrase: <b>{{$user['company']['catchPhrase']}}</b> <br>
                                Bs: <b>{{$user['company']['bs']}}</b> <br>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.js"></script>
<script>
    $('table').DataTable({
        "ordering": false
    });
</script>
</html>
